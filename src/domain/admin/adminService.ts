import { Repository } from "typeorm";
import { Admin } from "./entities/admin";
import { UpdateAdminDto } from "./dto/updateAdminDto";
import { CreateAdminDto } from "./dto/createAdminDto";
import { NotFoundError } from "../shared/errors/notFoundError";

export class AdminService {
  constructor(private readonly adminRepository: Repository<Admin>) {}

  public async create(createAdminDto: CreateAdminDto): Promise<Admin> {
    await createAdminDto.validate();

    const admin = await this.adminRepository.create(createAdminDto);

    return await this.adminRepository.save(admin);
  }

  public async update(
    id: number,
    updateAdminDto: UpdateAdminDto,
  ): Promise<void> {
    await updateAdminDto.validate();

    const admin = await this.adminRepository.findOne({ id });

    if (!admin) {
      throw new NotFoundError("Admin not found");
    }

    await this.adminRepository.update({ id }, updateAdminDto);
  }

  public async delete(id: number): Promise<void> {
    const admin = await this.adminRepository.findOne({ id });

    if (!admin) {
      throw new NotFoundError("Admin not found");
    }

    await this.adminRepository.delete({ id });
  }
}
