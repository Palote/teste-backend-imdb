import { Admin } from "./entities/admin";
import { Repository } from "typeorm";
import { ForbiddenError } from "../shared/errors/forbiddenError";
import { BadRequestError } from "../shared/errors/badRequestError";

export class AuthAdminService {
  constructor(private readonly adminRepository: Repository<Admin>) {}

  public async login(email: string, password: string): Promise<Admin> {
    if (!(email && password)) {
      throw new BadRequestError("Empty authentication credentials");
    }

    const admin = await this.adminRepository.findOne(
      { email },
      {
        select: ["id", "email", "password"],
      },
    );

    if (!admin || !admin.checkPassword(password)) {
      throw new ForbiddenError("Invalid authentication credentials");
    }

    return admin;
  }
}
