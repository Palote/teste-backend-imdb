import { UpdateAdminDto } from "./updateAdminDto";
import { IsString } from "class-validator";

export class CreateAdminDto extends UpdateAdminDto {
  @IsString()
  password: string;

  constructor(fullname: string, email: string, password: string) {
    super(fullname, email);

    this.password = password;
  }
}
