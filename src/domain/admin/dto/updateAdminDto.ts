import { IsString, IsEmail } from "class-validator";
import { BaseValidateClass } from "../../shared/validator/baseValidateClass";

export class UpdateAdminDto extends BaseValidateClass {
  @IsString()
  fullname: string;

  @IsEmail()
  email: string;

  constructor(fullname: string, email: string) {
    super();
    this.fullname = fullname;
    this.email = email;
  }
}
