import { Repository } from "typeorm";
import { User } from "./entities/user";
import { BadRequestError } from "../shared/errors/badRequestError";
import { ForbiddenError } from "../shared/errors/forbiddenError";

export class AuthUserService {
  constructor(private readonly userRepository: Repository<User>) {}

  public async login(email: string, password: string): Promise<User> {
    if (!(email && password)) {
      throw new BadRequestError("Empty authentication credentials");
    }

    const user = await this.userRepository.findOne(
      { email },
      {
        select: ["id", "email", "password"],
      },
    );

    if (!user || !user.checkPassword(password)) {
      throw new ForbiddenError("Invalid authentication credentials");
    }

    return user;
  }
}
