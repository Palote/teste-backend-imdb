import { Repository } from "typeorm";
import { User } from "./entities/user";
import { UpdateUserDto } from "./dto/updateUserDto";
import { CreateUserDto } from "./dto/createUserDto";
import { NotFoundError } from "../shared/errors/notFoundError";

export class UserService {
  constructor(private readonly userRepository: Repository<User>) {}

  public async create(createUserDto: CreateUserDto): Promise<User> {
    await createUserDto.validate();

    const user = this.userRepository.create(createUserDto);

    return await this.userRepository.save(user);
  }

  public async update(id: number, updateUserDto: UpdateUserDto): Promise<void> {
    await updateUserDto.validate();

    const user = await this.userRepository.findOne({ id });

    if (!user) {
      throw new NotFoundError("User dont exist");
    }

    await this.userRepository.update({ id }, updateUserDto);
  }

  public async delete(id: number): Promise<void> {
    const user = await this.userRepository.findOne({ id });

    if (!user) {
      throw new NotFoundError("user dont exist");
    }

    await this.userRepository.delete({ id });
  }
}
