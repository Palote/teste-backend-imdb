import { IsString, IsEmail } from "class-validator";
import { BaseValidateClass } from "../../shared/validator/baseValidateClass";

export class UpdateUserDto extends BaseValidateClass {
  @IsString()
  fullname: string;

  @IsEmail()
  email: string;

  constructor(fullname: string, email: string) {
    super();
    this.fullname = fullname;
    this.email = email;
  }
}
