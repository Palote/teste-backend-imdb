import { UpdateUserDto } from "./updateUserDto";
import { IsString } from "class-validator";

export class CreateUserDto extends UpdateUserDto {
  @IsString()
  password: string;

  constructor(fullname: string, email: string, password: string) {
    super(fullname, email);
    this.password = password;
  }
}
