import { validate } from "class-validator";
import { ClassValidationError } from "../errors/classValidationError";

export abstract class BaseValidateClass {
  public async validate(): Promise<void> {
    const errors = await validate(this);

    if (errors.length) {
      throw new ClassValidationError("Validation Error", errors);
    }
  }
}
