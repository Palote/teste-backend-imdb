import { ValidationError } from "class-validator";

export class ClassValidationError extends Error {
  details: ValidationError[];

  constructor(message: string, validationErrors: ValidationError[]) {
    super(message);
    this.details = validationErrors;
  }
}
