import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  ManyToMany,
  JoinTable,
  JoinColumn,
} from "typeorm";
import { Genre } from "./genre";
import { Person } from "./person";
import { Vote } from "./vote";

@Entity("movies")
export class Movie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  year: number;

  @ManyToMany((type) => Genre, { cascade: true })
  @JoinTable({
    name: "movie_genres",
    joinColumn: {
      name: "movie_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "genre_id",
      referencedColumnName: "id",
    },
  })
  genres: Genre[];

  @Column()
  description: string;

  @ManyToOne((type) => Person, (person) => person.id, { cascade: true })
  @JoinColumn({ name: "director_id" })
  director: Person;

  @ManyToMany((type) => Person, { cascade: true })
  @JoinTable({
    name: "movie_actors",
    joinColumn: {
      name: "movie_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "person_id",
      referencedColumnName: "id",
    },
  })
  actors: Person[];

  @OneToMany((type) => Vote, (vote) => vote.movie)
  votes: Vote[];
}
