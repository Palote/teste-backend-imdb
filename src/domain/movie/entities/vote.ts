import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { User } from "../../user/entities/user";
import { Movie } from "./movie";

@Entity("votes")
export class Vote {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne((type) => Movie, (movie) => movie.votes)
  @JoinColumn({ name: "movie_id" })
  movie: Movie;

  @ManyToOne((type) => User)
  @JoinColumn({ name: "user_id" })
  user: User;

  @Column()
  value: number;
}
