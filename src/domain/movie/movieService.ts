import { Repository } from "typeorm";
import { Movie } from "./entities/movie";
import { MovieDto } from "./dto/movieDto";
import { Genre } from "./entities/genre";
import { Person } from "./entities/person";
import { GenreRepository } from "./repositories/genreRepository";
import { PersonRepository } from "./repositories/personRepository";
import { NotFoundError } from "../shared/errors/notFoundError";

export class MovieService {
  constructor(
    private readonly movieRepository: Repository<Movie>,
    private readonly genreRepository: GenreRepository,
    private readonly personRepository: PersonRepository,
  ) {}

  public async list(): Promise<Movie[]> {
    const movies = await this.movieRepository.find();

    return movies;
  }

  public async get(id: number): Promise<Movie> {
    const movie = await this.movieRepository.findOne(
      { id },
      {
        relations: ["genres", "actors", "director", "votes"],
      },
    );

    if (!movie) {
      throw new NotFoundError("Movie not found");
    }

    return movie;
  }

  public async create(createMovieDto: MovieDto): Promise<Movie> {
    await createMovieDto.validate();

    const genreList = await this.findGenres(createMovieDto.genres);
    const actorList = await this.findActors(createMovieDto.actors);
    const director = await this.findDirector(createMovieDto.director);

    const movie = this.movieRepository.create({
      title: createMovieDto.title,
      description: createMovieDto.description,
      year: createMovieDto.year,
      director,
      genres: genreList,
      actors: actorList,
    });

    return await this.movieRepository.save(movie);
  }

  private async findGenres(genresNames: string[]): Promise<Genre[]> {
    const genreList: Genre[] = [];

    for (const genreName of genresNames) {
      const genre = await this.genreRepository.findOrCreateByName(genreName);
      genreList.push(genre);
    }

    return genreList;
  }

  private async findActors(actorsNames: string[]): Promise<Person[]> {
    const actorList: Person[] = [];

    for (const actorName of actorsNames) {
      const actor = await this.personRepository.findOrCreateByName(actorName);
      actorList.push(actor);
    }

    return actorList;
  }

  private async findDirector(directorName: string): Promise<Person> {
    return await this.personRepository.findOrCreateByName(directorName);
  }
}
