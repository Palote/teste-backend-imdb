import { IsString, IsInt, Min, IsArray, ArrayMinSize } from "class-validator";
import { BaseValidateClass } from "../../shared/validator/baseValidateClass";

export class MovieDto extends BaseValidateClass {
  @IsString()
  title: string;

  @IsInt()
  @Min(1900)
  year: number;

  @IsArray()
  @ArrayMinSize(1)
  @IsString({ each: true })
  genres: string[];

  @IsString()
  description: string;

  @IsString()
  director: string;

  @IsArray()
  @ArrayMinSize(1)
  @IsString({ each: true })
  actors: string[];

  constructor(
    title: string,
    year: number,
    genres: string[],
    description: string,
    director: string,
    actors: string[],
  ) {
    super();
    this.title = title;
    this.year = year;
    this.genres = genres;
    this.description = description;
    this.director = director;
    this.actors = actors;
  }
}
