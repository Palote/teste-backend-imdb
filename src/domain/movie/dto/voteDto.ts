import { IsInt, Min, Max } from "class-validator";
import { BaseValidateClass } from "../../shared/validator/baseValidateClass";

export class VoteDto extends BaseValidateClass {
  @IsInt()
  userId: number;

  @IsInt()
  movieId: number;

  @IsInt()
  @Min(0)
  @Max(4)
  value: number;

  constructor(userId: number, movieId: number, value: number) {
    super();
    this.userId = userId;
    this.movieId = movieId;
    this.value = value;
  }
}
