import { Repository } from "typeorm";
import { Vote } from "./entities/vote";
import { VoteDto } from "./dto/voteDto";
import { Movie } from "./entities/movie";
import { VoteRepository } from "./repositories/voteRepository";
import { NotFoundError } from "../shared/errors/notFoundError";

export class VoteService {
  constructor(
    private readonly voteRepository: VoteRepository,
    private readonly movieRepository: Repository<Movie>,
  ) {}

  public async vote(voteDto: VoteDto): Promise<Vote> {
    await voteDto.validate();

    await this.validateMovie(voteDto.movieId);

    const vote = await this.voteRepository.findOrCreate(
      voteDto.userId,
      voteDto.movieId,
      voteDto.value,
    );

    vote.value = voteDto.value;

    return await this.voteRepository.save(vote);
  }

  private async validateMovie(movieId: number): Promise<void> {
    const movie = await this.movieRepository.findOne({ id: movieId });

    if (!movie) {
      throw new NotFoundError("Movie not found");
    }
  }
}
