import { EntityRepository, Repository } from "typeorm";
import { Genre } from "../entities/genre";

@EntityRepository(Genre)
export class GenreRepository extends Repository<Genre> {
  public async findOrCreateByName(name: string): Promise<Genre> {
    const genre = await this.findOne({ name });

    if (genre) {
      return genre;
    }

    return this.create({
      name,
    });
  }
}
