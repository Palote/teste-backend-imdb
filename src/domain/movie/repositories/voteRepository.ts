import { Repository, EntityRepository } from "typeorm";
import { Vote } from "../entities/vote";

@EntityRepository(Vote)
export class VoteRepository extends Repository<Vote> {
  public async findOrCreate(
    userId: number,
    movieId: number,
    value: number,
  ): Promise<Vote> {
    const vote = await this.findOne({
      user: { id: userId },
      movie: { id: movieId },
    });

    if (vote) {
      return vote;
    }

    return this.create({
      user: { id: userId },
      movie: { id: movieId },
      value,
    });
  }
}
