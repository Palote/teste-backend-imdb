import { Repository, EntityRepository } from "typeorm";
import { Person } from "../entities/person";

@EntityRepository(Person)
export class PersonRepository extends Repository<Person> {
  public async findOrCreateByName(name: string): Promise<Person> {
    const person = await this.findOne({ name });

    if (person) {
      return person;
    }

    return this.create({
      name,
    });
  }
}
