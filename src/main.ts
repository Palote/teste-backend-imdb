import { expressServer } from "./infrastructure/express/expressServer";
import { connect } from "./infrastructure/typeorm/connection";

(async () => {
  await connect();
  expressServer(3000);
})();
