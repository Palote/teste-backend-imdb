import { AdminService } from "../../../domain/admin/adminService";
import { Request, Response } from "express";
import { UpdateAdminDto } from "../../../domain/admin/dto/updateAdminDto";
import { CreateAdminDto } from "../../../domain/admin/dto/createAdminDto";
import jwt from "jsonwebtoken";
import { AuthAdminService } from "../../../domain/admin/authAdminService";
import { jwtConfig } from "../../../config/jwt";

export class AdminController {
  constructor(
    private readonly adminService: AdminService,
    private readonly authAdminService: AuthAdminService,
  ) {}

  public async login(request: Request, response: Response): Promise<void> {
    const { email, password } = request.body;

    const admin = await this.authAdminService.login(email, password);

    const jwtToken = jwt.sign(
      { userId: admin.id, email: admin.email, context: "admin" },
      jwtConfig.jwtSecret,
    );

    response.status(200).json(jwtToken);
  }

  public async post(request: Request, response: Response): Promise<void> {
    const { fullname, email, password } = request.body;
    const createAdminDto = new CreateAdminDto(fullname, email, password);

    const admin = await this.adminService.create(createAdminDto);

    response.status(201).json({
      id: admin.id,
    });
  }

  public async put(request: Request, response: Response): Promise<void> {
    const id = Number(request.params.id);

    const { fullname, email } = request.body;
    const createAdminDto = new UpdateAdminDto(fullname, email);

    await this.adminService.update(id, createAdminDto);

    response.status(200).json({
      message: "Admin updated",
    });
  }

  public async delete(request: Request, response: Response): Promise<void> {
    const id = Number(request.params.id);

    await this.adminService.delete(id);

    response.status(200).json({
      message: "Admin deleted",
    });
  }
}
