import { MovieService } from "../../../domain/movie/movieService";
import { Response, Request } from "express";
import { MovieDto } from "../../../domain/movie/dto/movieDto";
import { VoteService } from "../../../domain/movie/voteService";
import { VoteDto } from "../../../domain/movie/dto/voteDto";

export class MovieController {
  constructor(
    private readonly movieService: MovieService,
    private readonly voteService: VoteService,
  ) {}

  public async list(_: Request, response: Response): Promise<void> {
    const movies = await this.movieService.list();
    response.status(200).json(movies);
  }

  public async get(request: Request, response: Response): Promise<void> {
    const movieId = Number(request.params.id);

    const movie = await this.movieService.get(movieId);

    response.status(200).json(movie);
  }

  public async create(request: Request, response: Response): Promise<void> {
    const { title, year, genres, description, director, actors } = request.body;

    const createMovieDto = new MovieDto(
      title,
      year,
      genres,
      description,
      director,
      actors,
    );

    const movie = await this.movieService.create(createMovieDto);

    response.status(201).json({
      id: movie.id,
    });
  }

  public async movieVote(request: Request, response: Response): Promise<void> {
    const { value } = request.body;
    const userId = Number(request.currentUser.userId);
    const movieId = Number(request.params.id);

    const voteDto = new VoteDto(userId, movieId, value);
    const vote = await this.voteService.vote(voteDto);

    response.status(201).json({
      id: vote.id,
    });
  }
}
