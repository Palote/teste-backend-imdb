import { UserService } from "../../../domain/user/userService";
import { Request, Response } from "express";
import { UpdateUserDto } from "../../../domain/user/dto/updateUserDto";
import { CreateUserDto } from "../../../domain/user/dto/createUserDto";
import { AuthUserService } from "../../../domain/user/userAdminService";
import { jwtConfig } from "../../../config/jwt";
import jwt from "jsonwebtoken";

export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly authUserService: AuthUserService,
  ) {}

  public async login(request: Request, response: Response): Promise<void> {
    const { email, password } = request.body;

    const admin = await this.authUserService.login(email, password);

    const jwtToken = jwt.sign(
      { userId: admin.id, email: admin.email, context: "plataform" },
      jwtConfig.jwtSecret,
    );

    response.status(200).json(jwtToken);
  }

  public async post(request: Request, response: Response): Promise<void> {
    const { fullname, email, password } = request.body;
    const createUserDto = new CreateUserDto(fullname, email, password);

    const user = await this.userService.create(createUserDto);

    response.status(201).json({
      id: user.id,
    });
  }

  public async put(request: Request, response: Response): Promise<void> {
    const id = Number(request.params.id);

    const { fullname, email } = request.body;
    const createUserDto = new UpdateUserDto(fullname, email);

    await this.userService.update(id, createUserDto);

    response.status(200).json({
      message: "User updated",
    });
  }

  public async delete(request: Request, response: Response): Promise<void> {
    const id = Number(request.params.id);

    await this.userService.delete(id);

    response.status(200).json({
      message: "User deleted",
    });
  }
}
