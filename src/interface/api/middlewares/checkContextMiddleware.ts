import { Request, Response, NextFunction } from "express";

export const checkContextMiddleware = (
  context: string,
): ((request: Request, response: Response, next: NextFunction) => void) => {
  return (request: Request, response: Response, next: NextFunction): void => {
    if (request.currentUser.context !== context) {
      response.status(403).json({
        message: "Permission denied",
      });
    }

    next();
  };
};
