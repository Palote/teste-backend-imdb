import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { jwtConfig } from "../../../config/jwt";
import { UserRequest } from "../../../infrastructure/express";

export const authorizationMiddleware = (
  request: Request,
  response: Response,
  next: NextFunction,
): void => {
  const token = <string>request.headers["x-access-token"];

  try {
    const jwtPayload = jwt.verify(token, jwtConfig.jwtSecret);

    request.currentUser = jwtPayload as UserRequest;
  } catch (error) {
    response.status(401).json({
      message: "Unauthorized",
    });
  }

  next();
};
