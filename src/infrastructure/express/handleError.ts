import { Request, Response, NextFunction } from "express";
import { NotFoundError } from "../../domain/shared/errors/notFoundError";
import { ForbiddenError } from "../../domain/shared/errors/forbiddenError";
import { ClassValidationError } from "../../domain/shared/errors/classValidationError";
import { BadRequestError } from "../../domain/shared/errors/badRequestError";

export const handleError = (
  error: Error,
  _: Request,
  response: Response,
  next: NextFunction,
): void => {
  if (error instanceof NotFoundError) {
    response.status(404).json({
      message: error.message,
    });
  } else if (error instanceof ForbiddenError) {
    response.status(403).json({
      message: error.message,
    });
  } else if (error instanceof BadRequestError) {
    response.status(400).json({
      message: error.message,
    });
  } else if (error instanceof ClassValidationError) {
    response.status(400).json({
      message: error.message,
      details: error.details,
    });
  } else {
    response.status(500).json({
      message: "Unexpected error",
    });
  }

  next();
};
