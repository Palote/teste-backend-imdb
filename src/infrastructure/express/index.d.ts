export interface UserRequest {
  userId: number;
  email: string;
  context: "admin" | "plataform";
}

declare global {
  namespace Express {
    interface Request {
      currentUser: UserRequest;
    }
  }
}
