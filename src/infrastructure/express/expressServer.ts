import express from "express";
import { addRoutes } from "./routes";
import bodyParser from "body-parser";
import { handleError } from "./handleError";

export const expressServer = (port: number): void => {
  const app = express();

  app.use(bodyParser.json());

  addRoutes(app);

  app.use(handleError);

  app.listen(port, () => {
    console.log(`Express server listening in PORT ${port}`);
  });
};
