import { UserService } from "../../../domain/user/userService";
import { Application } from "express";
import { getRepository } from "typeorm";
import { User } from "../../../domain/user/entities/user";
import { UserController } from "../../../interface/api/controllers/userController";
import { AuthUserService } from "../../../domain/user/userAdminService";
import expressAsyncHandler from "express-async-handler";

export const userRoutes = (server: Application): void => {
  const userRepository = getRepository(User);
  const userService = new UserService(userRepository);
  const authUserService = new AuthUserService(userRepository);
  const userController = new UserController(userService, authUserService);

  server.post(
    "/user",
    expressAsyncHandler(userController.post.bind(userController)),
  );
  server.put(
    "/user/:id",
    expressAsyncHandler(userController.put.bind(userController)),
  );
  server.delete(
    "/user/:id",
    expressAsyncHandler(userController.delete.bind(userController)),
  );
  server.post(
    "/user/login",
    expressAsyncHandler(userController.login.bind(userController)),
  );
};
