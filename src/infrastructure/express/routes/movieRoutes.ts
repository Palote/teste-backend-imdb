import { MovieService } from "../../../domain/movie/movieService";
import { getRepository, getCustomRepository } from "typeorm";
import { Movie } from "../../../domain/movie/entities/movie";
import { Application } from "express";
import { MovieController } from "../../../interface/api/controllers/movieController";
import { GenreRepository } from "../../../domain/movie/repositories/genreRepository";
import { PersonRepository } from "../../../domain/movie/repositories/personRepository";
import { VoteService } from "../../../domain/movie/voteService";
import { VoteRepository } from "../../../domain/movie/repositories/voteRepository";
import { authorizationMiddleware } from "../../../interface/api/middlewares/authorizationMiddleware";
import { checkContextMiddleware } from "../../../interface/api/middlewares/checkContextMiddleware";
import expressAsyncHandler from "express-async-handler";

export const movieRoutes = (server: Application): void => {
  const movieRepository = getRepository(Movie);
  const genreRepository = getCustomRepository(GenreRepository);
  const personRepository = getCustomRepository(PersonRepository);
  const voteRepository = getCustomRepository(VoteRepository);

  const movieService = new MovieService(
    movieRepository,
    genreRepository,
    personRepository,
  );
  const voteService = new VoteService(voteRepository, movieRepository);
  const movieController = new MovieController(movieService, voteService);

  server.get(
    "/movie",
    expressAsyncHandler(movieController.list.bind(movieController)),
  );
  server.get(
    "/movie/:id",
    expressAsyncHandler(movieController.get.bind(movieController)),
  );
  server.post(
    "/movie",
    [authorizationMiddleware, checkContextMiddleware("admin")],
    expressAsyncHandler(movieController.create.bind(movieController)),
  );
  server.put(
    "/movie/:id/vote",
    [authorizationMiddleware, checkContextMiddleware("plataform")],
    expressAsyncHandler(movieController.movieVote.bind(movieController)),
  );
};
