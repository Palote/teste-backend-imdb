import { Application } from "express";
import { adminRoutes } from "./adminRoutes";
import { userRoutes } from "./userRoutes";
import { movieRoutes } from "./movieRoutes";

export const addRoutes = (server: Application): void => {
  adminRoutes(server);
  userRoutes(server);
  movieRoutes(server);
};
