import { Application } from "express";
import { AdminController } from "../../../interface/api/controllers/adminController";
import { AdminService } from "../../../domain/admin/adminService";
import { Admin } from "../../../domain/admin/entities/admin";
import { getRepository } from "typeorm";
import { AuthAdminService } from "../../../domain/admin/authAdminService";
import expressAsyncHandler from "express-async-handler";

export const adminRoutes = (server: Application): void => {
  const adminRepository = getRepository(Admin);
  const adminService = new AdminService(adminRepository);
  const authAdminService = new AuthAdminService(adminRepository);
  const adminController = new AdminController(adminService, authAdminService);

  server.post(
    "/admin",
    expressAsyncHandler(adminController.post.bind(adminController)),
  );
  server.put(
    "/admin/:id",
    expressAsyncHandler(adminController.put.bind(adminController)),
  );
  server.delete(
    "/admin/:id",
    expressAsyncHandler(adminController.delete.bind(adminController)),
  );
  server.post(
    "/admin/login",
    expressAsyncHandler(adminController.login.bind(adminController)),
  );
};
