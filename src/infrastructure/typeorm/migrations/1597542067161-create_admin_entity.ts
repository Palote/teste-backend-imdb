import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class createAdminEntity1597542067161 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "admins",
        columns: [
          {
            name: "id",
            isPrimary: true,
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "fullname",
            type: "varchar",
          },
          {
            name: "email",
            type: "varchar",
          },
          {
            name: "password",
            type: "varchar",
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("admins");
  }
}
