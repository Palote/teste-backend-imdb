import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from "typeorm";

export class createMovieEntity1597620106850 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "genres",
        columns: [
          {
            name: "id",
            isPrimary: true,
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "name",
            type: "varchar",
          },
        ],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: "persons",
        columns: [
          {
            name: "id",
            isPrimary: true,
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "name",
            type: "varchar",
          },
        ],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: "movies",
        columns: [
          {
            name: "id",
            isPrimary: true,
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "title",
            type: "varchar",
          },
          {
            name: "year",
            type: "int",
          },
          {
            name: "description",
            type: "text",
          },
          {
            name: "director_id",
            type: "int",
          },
        ],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: "movie_actors",
        columns: [
          {
            name: "movie_id",
            type: "int",
            isPrimary: true,
          },
          {
            name: "person_id",
            type: "int",
            isPrimary: true,
          },
        ],
      }),
    );

    await queryRunner.createForeignKeys("movie_actors", [
      new TableForeignKey({
        name: "movie_actor_movie_fk",
        columnNames: ["movie_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "movies",
      }),
      new TableForeignKey({
        name: "movie_actor_person_fk",
        columnNames: ["person_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "persons",
      }),
    ]);

    await queryRunner.createTable(
      new Table({
        name: "movie_genres",
        columns: [
          {
            name: "movie_id",
            type: "int",
            isPrimary: true,
          },
          {
            name: "genre_id",
            type: "int",
            isPrimary: true,
          },
        ],
      }),
    );

    await queryRunner.createForeignKeys("movie_genres", [
      new TableForeignKey({
        name: "movie_genre_movie_fk",
        columnNames: ["movie_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "movies",
      }),
      new TableForeignKey({
        name: "movie_genre_genre_fk",
        columnNames: ["genre_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "genres",
      }),
    ]);

    await queryRunner.createTable(
      new Table({
        name: "votes",
        columns: [
          {
            name: "id",
            isPrimary: true,
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "movie_id",
            type: "int",
            isPrimary: true,
          },
          {
            name: "user_id",
            type: "int",
            isPrimary: true,
          },
          {
            name: "value",
            type: "int",
          },
        ],
      }),
    );

    await queryRunner.createForeignKeys("votes", [
      new TableForeignKey({
        name: "vote_movie_fk",
        columnNames: ["movie_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "movies",
      }),
      new TableForeignKey({
        name: "vote_user_fk",
        columnNames: ["user_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "users",
      }),
    ]);

    await queryRunner.createForeignKey(
      "movies",
      new TableForeignKey({
        name: "movie_director_fk",
        columnNames: ["director_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "persons",
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey("movies", "movie_director_fk");

    await queryRunner.dropForeignKey("votes", "vote_movie_fk");
    await queryRunner.dropForeignKey("votes", "vote_user_fk");
    await queryRunner.dropTable("votes");

    await queryRunner.dropForeignKey("movie_genres", "movie_genre_movie_fk");
    await queryRunner.dropForeignKey("movie_genres", "movie_genre_genre_fk");
    await queryRunner.dropTable("movie_genres");

    await queryRunner.dropForeignKey("movie_actors", "movie_actor_movie_fk");
    await queryRunner.dropForeignKey("movie_actors", "movie_actor_person_fk");
    await queryRunner.dropTable("movie_actors");

    await queryRunner.dropTable("movies");
    await queryRunner.dropTable("persons");
    await queryRunner.dropTable("genres");
  }
}
