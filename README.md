# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto de avaliação de candidatos.

# 🏗 O que fazer?

- Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

# 🚨 Requisitos

- A API deverá ser construída em **NodeJS** ou **Rails**
- Implementar autenticação e deverá seguir o padrão **JWT**, lembrando que o token a ser recebido deverá ser no formato **Bearer**
- Caso seja desenvolvida em NodeJS o seu projeto terá que ser implementado em **ExpressJS** ou **SailsJS**
- Para a comunicação com o banco de dados utilize algum **ORM**/**ODM**
- Bancos relacionais permitidos:
  - MySQL
  - MariaDB
  - Postgre
- Bancos não relacionais permitidos:
  - MongoDB
- Sua API deverá seguir os padrões Rest na construção das rotas e retornos
- Sua API deverá conter a collection/variáveis do postman ou algum endpoint da documentação em openapi para a realização do teste

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do Projeto
- Segurança da API, como autenticação, senhas salvas no banco, SQL Injection e outros
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir tudo o que foi exigido na seção [O que desenvolver?](##--o-que-desenvolver)
- Migrations para a criação das tabelas do banco relacional

# 🎁 Extra

Esses itens não são obrigatórios, porém desejados.

- Testes unitários
- Linter
- Code Formater

**Obs.: Lembrando que o uso de algum linter ou code formater irá depender da linguagem que sua API for criada**

# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deve conter as seguintes features:

- Admin

  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Usuário

  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Filmes

  - Cadastro (Somente um usuário administrador poderá realizar esse cadastro)
  - Voto (A contagem dos votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
  - Listagem (deverá ter filtro por diretor, nome, gênero e/ou atores)
  - Detalhe do filme trazendo todas as informações sobre o filme, inclusive a média dos votos

**Obs.: Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é admin ou não**

# 🔗 Links

- Documentação JWT https://jwt.io/
- Frameworks NodeJS:

  1. https://expressjs.com/pt-br/
  2. https://sailsjs.com/

- Guideline rails http://guides.rubyonrails.org/index.html

## Arquitetura do projeto
O projeto foi dividido nas seguintes camadas
- Infrastructure: implementação dos serviços externos ao domínio da aplicação, como banco de dados e servidor http (Express);
- Interface: implementação das interfaces de comunicação com o serviço, como API Rest, GraphQl, CLI e etc.
- Domain: implementação do domínio da aplicação.

Essa arquitetura busca uma proximidade com o conceito de Clean Architecture. Apesar de não estar seguintes os conceitos, buscar só depender de abstrações e etc. As camadas de domínio, interface e infraestrutura estão separados, facilitando uma futura refatoração para uma implementação fiel a esses conceitos.
Um ponto relevante que atualmente quebra bastante o conceito de Clean Architecture é o uso do TypeORM direto nas entidades, que deixa a camada de domínio bastante acoplada a um serviço (lib) externa ao domínio da nossa aplicação.4

## Tecnologias utilizadas no projeto
- typescript
- typeorm
- express
- class-validator
- eslint
- prettier
- ts-node

## Como rodar esse projeto
- Instalar todas as depedências dos projeto utilizando os comandos `yarn install` ou `npm install`;
- Adicionar as configurações referentes ao banco relacional a ser usado. Para teste e desenvolvimento foi utilizado o PostgreSQL, mas visto o uso do Typeorm, pode ser utilizado outras soluções de banco relacional, como MySQL. O arquivo de configuração se encontra `./ormconfig.json`;
- Após a configuração do banco de dados, executar o comando `yarn migration:run` ou `npm run migration:run`, para executar todas as migrations no banco configurado;
- Após a execução das migrations do projeto, basta executar o comando `yarn dev:start` ou `npm run dev:start` para iniciar o projeto, que será exposto na porta `3000`;

## Documentação de rotas e testes
As collections e variáveis do Postman se encontram na pasta `./postman`.

## Outros comandos relavantes
- `yarn lint` ou `npm run lint` para executar a analise do lint no projeto;
- `yarn format` ou `npm run format` para rodar o prettier no projeto;
- `yarn build` ou `npm run build` para compilar o projeto;

## Pontos a se melhorar ou faltantes no projeto
- Implementação dos filtros na rota de listagem de filmes;
- Implementação da média de votos na busca por filme;
- Controle de acesso nas rotas de atualização de Admin e Usuários;
- Implementação da redefinição de senha para Admin e Usuários;
- Análise sobre a junção entre os domínios de Admin e Usuário. São domínios diferentes ou não?
- Uso de `.env` para informações como jwtSecret;
- Implementação de testes unitários na camada de domínio da aplicação;
- Implementação de testes de integração / api nos endpoints da aplicação;
- Implementar docker para aplicação;
- Melhorar uso das injeções de depedências do projeto. Implementar container de injeção e remover implementação da definição das rotas.
